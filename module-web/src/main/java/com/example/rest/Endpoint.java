package com.example.rest;

import com.example.jar.ExampleService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/hello")
@RequestScoped
public class Endpoint {

   @Inject
   ExampleService exampleService;

   @GET
   public String hello() {
      return "Service call: " + exampleService.getName();
   }
}
