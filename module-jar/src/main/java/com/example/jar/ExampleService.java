package com.example.jar;

import javax.enterprise.context.RequestScoped;

@RequestScoped
public class ExampleService {

    public String getName() {
        return "Hello example service";
    }
}
